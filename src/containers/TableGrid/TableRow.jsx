import React from 'react';
import {connect} from 'react-redux';
import {Table} from '@devexpress/dx-react-grid-material-ui';
import {openModalProject} from "../../store/actions/rssProjects";

const styles = {
	banking: {
		backgroundColor: '#f5f5f5',
	},
	health: {
		backgroundColor: '#a2e2a4',
	},
	telecom: {
		backgroundColor: '#b3e5fc',
	},
	energy: {
		backgroundColor: '#ffcdd2',
	},
	insurance: {
		backgroundColor: '#f0f4c3',
	},
};

const TableRow = ({props, row, ...restProps }) => (
	<Table.Row
		{...restProps}
		// eslint-disable-next-line no-alert
		// onClick={() => window.location.href = row.id }
		onClick={() => {


			return restProps.openModalProject(true,row)
			}
		}
		style={{
			cursor: 'pointer',
			...styles,
		}}
	/>
);

const mapStateToProps = (state) => {
	return {
		modalProject: state.rssProjects.modalProject,
		projects: state.rssProjects.projects,
		loading: state.rssProjects.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return{
		openModalProject: (status,project) => dispatch(openModalProject(status,project))
	}
};

export default connect(mapStateToProps,mapDispatchToProps)(TableRow);