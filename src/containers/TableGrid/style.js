import {makeStyles} from "@material-ui/core/styles/index";

export const useStyles = makeStyles(theme => ({
	root:{

		'& .MuiTableCell-root:nth-last-child(4)': {
			width: '30%'
		},

		'& .MuiTableCell-root:nth-last-child(3)': {
			paddingRight: '16px',
			width: '10%'
		},

		'& .MuiTableCell-root:nth-last-child(2)': {
			fontSize: 0,
			width: '1px',
			pointerEvents: 'none',
			padding: 0,
			margin: 0,
			visibility: 'hidden',
			transform: 'scaleX(0)'
		},

		'& .MuiTableCell-root:last-child': {
			fontSize: 0,
			width: '1px',
			pointerEvents: 'none',
			padding: 0,
			margin: 0,
			visibility: 'hidden',
			transform: 'scaleX(0)'
		},

		'& .MuiListItem-gutters': {
			padding: 0
		},

		'& .Editor-input': {
			maxWidth: '240px'
		},

		'& .MuiInput-underline:before': {
			maxWidth: '240px'
		},

		'& .MuiInput-underline:after': {
			maxWidth: '240px'
		}
	},
	paper: {
		backgroundColor: '#fff',
		borderRadius: '6px',
		margin: 0,
		padding: 0,
		boxShadow: '0 0 3px rgba(0,0,0,.1)'
	},
	sidebar: {
		backgroundColor: '#fff',
		borderRadius: '6px',
		boxShadow: '0 0 3px rgba(0,0,0,.1)'
	}
}));