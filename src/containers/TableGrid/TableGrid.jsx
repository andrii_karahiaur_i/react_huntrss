import React, {useState,Fragment} from 'react';
import {connect} from 'react-redux';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import TableRow from './TableRow';
import {useStyles} from './style';

import {
    FilteringState,
    IntegratedFiltering,
    PagingState,
    IntegratedPaging
} from '@devexpress/dx-react-grid';
import {
    Grid,
    Table,
    TableHeaderRow,
    TableFilterRow,
    PagingPanel
} from '@devexpress/dx-react-grid-material-ui';
import {
	fetchGetProjectsRss
} from "../../store/actions/rssProjects";

import InputTextField from '../../components/InputTextField/index';
import DialogsModal from "../../components/DialogsModal/index";

const TableGrid = props => {
    const classes = useStyles();
    const [values, setValues] = useState({
        description: ''
    });

    const columns = [
        { name: 'name', title: 'Название проекта' },
        { name: 'category', title: 'Категория проекта' },
        { name: 'price', title: 'Бюджет' },
        { name: 'description', title: 'description' },
        { name: 'id', title: 'ID' }
    ];

	const renderRows = (rowsData) => { // props.projects
		try {
            let data = [];
			let filtered = null;
			let search = values.description.trim().toLowerCase();

            rowsData.map(row => {
                let price = row.title.indexOf('₴') > -1 || row.title.indexOf('₽ ') > -1
                    ?
                    row.title.split(" - ").pop()
                    :
                    '';
	            return data.push(
                    {name: row.title, category: row.categories[0].name, price: price, id: row.id, description: row.description}
                );
            });

            // filtered

			if(search.length > 0) {

                let data = [];
                filtered = rowsData.filter((project) => {
	                return project.description.toLowerCase().includes(search);
                });

				filtered.map(row => {
                    let price = row.title.indexOf('₴') > -1 || row.title.indexOf('₽ ') > -1
                        ?
                        row.title.split(" - ").pop()
                        :
                        '';
                    data.push(
                        {name: row.title, category: row.categories[0].name, price: price, id: row.id, description: row.description}
                    );
	            });

				return data;
			}else{
				return data;
			}

		}
		catch(e){
			console.error(e)
		}
	};

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    return (
	    <Fragment>

		    <div className={classes.root}>
			    <Box display="flex" justifyContent="space-between">
				    <Box width="20%" p={2} className={classes.sidebar} >
					    <List component="nav" aria-label="main mailbox folders">
						    <ListItem>
							    <Typography variant="subtitle1">
								    Фильтры
							    </Typography>
						    </ListItem>
						    <Divider />
						    <ListItem>
							    <InputTextField
								    id="standard-name"
								    label="Ключевая фраза"
								    value={values.name}
								    onChange={handleChange('description')}
								    margin="normal"
							    />
						    </ListItem>
					    </List>

				    </Box>
				    <Box width="80%" px={2}>
					    <Paper className={classes.paper}>
						    {
							    renderRows(props.projects).length !== 0
								    ?
								    <Grid
									    rows={ renderRows(props.projects) }
									    columns={ columns }
								    >
									    <FilteringState defaultFilters={[]} />
									    <PagingState
										    defaultCurrentPage={0}
										    pageSize={12}
									    />
									    <IntegratedFiltering />
									    <IntegratedPaging />
									    <Table
										    rowComponent={TableRow}
                                        />
									    <TableHeaderRow />
									    <TableFilterRow />
									    <PagingPanel />
								    </Grid>
								    : null
						    }
                        </Paper>
                    </Box>
                </Box>
            </div>
		    <DialogsModal />
        </Fragment>
    );
};

const mapStateToProps = (state) => {
    return {
	    modalProject: state.rssProjects.modalProject,
	    projects: state.rssProjects.projects,
	    loading: state.rssProjects.loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return{
        fetchGetProjectsRss: () => dispatch(fetchGetProjectsRss()),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(TableGrid);
