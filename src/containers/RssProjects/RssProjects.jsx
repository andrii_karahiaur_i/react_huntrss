import React, {useEffect } from 'react';
import {connect} from 'react-redux';
import {fetchGetProjectsRss} from '../../store/actions/rssProjects';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import FlashOnIcon from '@material-ui/icons/FlashOn';

import CircularProgress from '../../components/CircularLoading/index';
import TableGrid from "../TableGrid/TableGrid";
import {useStyles} from './style';

const RssProjects = props => {
	const classes = useStyles();

	useEffect( () => {
		props.fetchGetProjectsRss();
	}, [fetchGetProjectsRss]);

	const rss = props.rss;
	return(
		<div className={classes.root}>
			{
				props.loading
				?
					<CircularProgress />
				:
					<Container maxWidth="lg" >
						<Box py={3}>
							<Typography component="h1">
								{rss ? rss.title : null}

								<Chip
									label={rss ? rss.items.length : null}
									disabled
									avatar={<FlashOnIcon/>}
								/>
							</Typography>
							<TableGrid />
						</Box>
					</Container>
			}

		</div>

	);
};

const mapStateToProps = (state) => {
	return {
		rss: state.rssProjects.rss,
		loading: state.rssProjects.loading,
	}
};

const mapDispatchToProps = (dispatch) => {
	return{
		fetchGetProjectsRss: () => dispatch(fetchGetProjectsRss())
	}
};

export default connect(mapStateToProps,mapDispatchToProps)(RssProjects);