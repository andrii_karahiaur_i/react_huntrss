import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {

			'& h1.MuiTypography-body1': {
				fontFamily: 'sans-serif',
				fontStyle: 'normal',
				fontWeight: 400,
				fontSize: '1.2rem',
				letterSpacing: '0',
				lineHeight: '1',
				color: '#555',
				flex: '1 1',
				marginTop:  '0.5rem',
				marginBottom:  '1rem',
			},

			'& .MuiChip-root':{
				minWidth: '74px',
				margin: '0 8px',
				backgroundColor: '#fafafa',
				boxShadow: '0 0 3px rgba(0,0,0,.1)'
			},

			'& .MuiChip-avatar': {
				width: '20px',
				height: '20px',
				fill: '#ffb233'
			},

			'& .MuiChip-label':{
				fontSize: '1rem',
				fontWeight: 700
			}
		}
	})
);

