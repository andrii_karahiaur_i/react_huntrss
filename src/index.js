import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import CssBaseline from '@material-ui/core/CssBaseline';

import reducer from './store/reducers/index';
import App from './App';

import * as serviceWorker from './serviceWorker';

const store = createStore(
	reducer,
	composeWithDevTools(applyMiddleware(thunk))
);

if( document.getElementById('root') )
	ReactDOM.render(
		<Provider store={store}>
			<CssBaseline />
			<App />
		</Provider>,
		document.getElementById('root')
	);

// Serve
serviceWorker.unregister();
