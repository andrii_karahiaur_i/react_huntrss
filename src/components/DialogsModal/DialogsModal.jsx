import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import GavelIcon from '@material-ui/icons/Gavel';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

import {styles} from './style';
import {openModalProject} from "../../store/actions/rssProjects";
import {connect} from "react-redux";

const DialogTitle = withStyles(styles)(props => {
	const { children, classes, onClose } = props;
	return (
		<MuiDialogTitle disableTypography className={classes.root}>
			<Typography variant="h6">{children}</Typography>
			{onClose ? (
				<IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
					<CloseIcon />
				</IconButton>
			) : null}
		</MuiDialogTitle>
	);
});

const DialogContent = withStyles(theme => ({
	root: {
		padding: theme.spacing(2),
	},
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
	root: {
		margin: 0,
		padding: theme.spacing(1),
	},
}))(MuiDialogActions);

const DialogsModal = props => {
	const handleClose = () => {
		props.openModalProject(false)
	};

	return (
		<div>
			{console.log( 'MODAL -- ', props.project )}
			<Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={props.modalProject}>
				<DialogTitle id="customized-dialog-title" onClose={handleClose}>
					{props.project ? props.project.name : null}
				</DialogTitle>
				<DialogContent dividers>

					<Chip
						label={props.project ? props.project.category : null}
						disabled
						style={{marginBottom: '1rem'}}
					/>
					<Typography gutterBottom>
						{props.project ? props.project.description : null}
					</Typography>

					<DialogActions>
						<Button variant="contained" href={props.project ? props.project.id : null} color="primary">
							<GavelIcon />
							<span style={{marginLeft: '0.5rem'}}>Сделать ставку</span>
						</Button>
					</DialogActions>
				</DialogContent>
			</Dialog>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		modalProject: state.rssProjects.modalProject,
		project: state.rssProjects.project
	}
};

const mapDispatchToProps = (dispatch) => {
	return{
		openModalProject: status => dispatch(openModalProject(status))
	}
};

export default connect(mapStateToProps,mapDispatchToProps)(DialogsModal);