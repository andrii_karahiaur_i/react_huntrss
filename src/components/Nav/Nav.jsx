import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import MenuIcon from '@material-ui/icons/Menu';

import {useStyles} from './style';
import logo from './freelancehunt-ru-ua.png';

const Nav = () => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<AppBar position="static">
				<Container maxWidth="lg" >
					<Box display="flex" justifyContent="space-between">
						<Toolbar>
							<a href="/" className={classes.logoBtn}>
								<img src={logo} alt="logo"/>
							</a>
						</Toolbar>
						<IconButton
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="open drawer"
						>
							<MenuIcon />
						</IconButton>
					</Box>
				</Container>
			</AppBar>
		</div>
	);
};

export default Nav;