import { fade, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,

		'& .MuiToolbar-gutters': {
			paddingLeft: 0
		},

		'& .MuiAppBar-colorPrimary': {
			backgroundColor: '#fbfbfb',
			boxShadow: '0 0 3px rgba(0,0,0,.1)',
			paddingTop: '8px',
			paddingBottom: '8px',
		},

		'& .MuiButtonBase-root ': {
			boxShadow: '0 0 3px rgba(0,0,0,.1)'
		},

		'& .MuiIconButton-label svg': {
			fill: '#ffb233'
		},

		'& .MuiIconButton-root:hover': {
			backgroundColor: '#ffb233',

			'& .MuiIconButton-label svg': {
				fill: '#fff'
			},
		}
	},
	menuButton: {
		flexBasis: '65px',
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
		display: 'none',
		[theme.breakpoints.up('sm')]: {
			display: 'block',
		},
	},
	logoBtn: {
		display: 'block',
		maxWidth: '300px',

		'& img': {
			display: 'block',
			width: '100%'
		}
	}

}));