import React from 'react';
import clsx from 'clsx';
import TextField from '@material-ui/core/TextField';
import {useStyles} from './style';

const  InputTextFields = props => {
	const classes = useStyles();
	return (
		<TextField
			id={props.id}
			label={props.label}
			className={clsx(classes.textField, classes.fontSize14)}
			value={props.value}
			onChange={props.onChange}
			margin={props.margin}
		/>
	)
};

export default InputTextFields;