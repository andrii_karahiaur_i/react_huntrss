import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
	root: {


	},

	textField: {
		width: '100%',
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1),

		'& .MuiFormLabel-root.Mui-focused': {
			color: '#51a351'
		},

		'& .MuiInput-underline:after': {
			borderBottomColor: 	'#51a351'
		},

		'& .MuiInput-underline:hover:not(.Mui-disabled):before': {
			borderBottomColor: '#ff8c00'
		}
	},
	fontSize14: {
		fontSize: '0.875rem',

		'& .MuiFormLabel-root': {
			fontSize: '0.875rem'
		}
	}
}));