import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import clsx from 'clsx';

const variables = {
	color: {
		'orange': '#ffb233',
		'white': '#fff'
	}
};

const useStyles = makeStyles(theme => ({
	root: {
		'& .MuiCircularProgress-circle': {
			stroke: variables.color.orange
		}
	},
	root_position: {
		position: 'fixed',
		top: '0',
		left: '0',
		zIndex: 999,
		backgroundColor: variables.color.white,
		width: '100%',
		height: '100%',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	progress: {
		margin: theme.spacing(2),
	},
}));

export default function CircularLoading() {
	const classes = useStyles();

	return (
		<div className={clsx(classes.root, classes.root_position)}>
			<CircularProgress className={classes.progress} />
		</div>
	);
}