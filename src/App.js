import React, {Fragment} from 'react';
import RssProjects from './containers/RssProjects';
import Nav from './components/Nav/index';

function App() {
  return (
      <Fragment>
          <Nav />
          <main>
            <RssProjects/>
          </main>
      </Fragment>
  );
}

export default App;
