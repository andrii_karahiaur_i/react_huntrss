import axios from 'axios';
import * as rssParser from 'react-native-rss-parser';

import {
	FETCH_PROJECT_RSS_SUCCESS,
	FETCH_RSS_START,
	FETCH_RSS_ERROR,
	FETCH_PROJECT_ITEMS_RSS, MODAL_PROJECT
} from './actionsType';

export const fetchGetProjectsRss = () => {
	return async dispatch => {
		dispatch(fetchRssStart());
		try{
			const response = await axios.get('/projects.rss');
			rssParser.parse(response.data).then( (rss) => dispatch(fetchProjectsRssSuccess(rss)) );
			rssParser.parse(response.data).then( (rss) => dispatch(fetchProjectsItemsRss(rss['items'])) );
		}
		catch(e){
			dispatch(fetchRssError(e))
		}
	}
};

export const openModalProject = (status,project) => {
	return{
		type: MODAL_PROJECT,
		status,
		project
	}
};

export const fetchProjectsItemsRss = (projects) => {
	return{
		type: FETCH_PROJECT_ITEMS_RSS,
		projects
	}
};

export const fetchProjectsRssSuccess = (rssProjects) => {
	return{
		type: FETCH_PROJECT_RSS_SUCCESS,
		rssProjects
	}
};

const fetchRssStart = () => {
	return{
		type: FETCH_RSS_START
	}
};


const fetchRssError = (error) => {
	return{
		type: FETCH_RSS_ERROR,
		error
	}
};