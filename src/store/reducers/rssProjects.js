import {
	FETCH_PROJECT_RSS_SUCCESS,
	FETCH_RSS_START,
	FETCH_RSS_ERROR,
	FETCH_PROJECT_ITEMS_RSS, MODAL_PROJECT,
} from '../actions/actionsType';

const initaState = {
	projects: [],
	project: {},
	loading: false,
	error: null,
	modalProject: false,
};

export const rssReducers = (state = initaState, action) => {
	switch(action.type){
		case FETCH_RSS_START:
			return{
				...state,
				loading: true
			};
		case FETCH_PROJECT_RSS_SUCCESS:
			return{
				...state,
				loading: false,
				rss: action.rssProjects
			};
		case FETCH_PROJECT_ITEMS_RSS:
			return{
				...state,
				loading: false,
				projects: action.projects
			};
		case FETCH_RSS_ERROR:
			return{
				...state,
				loading: false,
				error: action.error
			};
		case MODAL_PROJECT:
			return{
				...state,
				modalProject: action.status,
				project: action.project
			};
		default:
			return state;
	}
};