import {combineReducers} from 'redux';
import {rssReducers} from './rssProjects';

export default combineReducers({
	rssProjects: rssReducers
});