import {rssReducers} from './rssProjects';
import {fetchProjectsRssSuccess,fetchProjectsItemsRss} from '../actions/rssProjects';


it('rss state should be added', () => {
// 1. start data
	const initaState = {};
// 2. action
	let action = fetchProjectsRssSuccess({
		rss: {title:"add All rss object"}
	});
	let newState = rssReducers(initaState, action);
// 3. expectation
	expect(Object.keys(newState.rss)).toBeTruthy()
});

it('projects state should be added', () => {
// 1. start data
	const initaState = {
		projects:[]
	};
// 2. action
	let action = fetchProjectsItemsRss({id: 'https...'});
	let newState = rssReducers(initaState, action);
// 3. expectation
	expect(newState.projects.length).not.toBe(0);
});