### RSS Projects *[Скачать](https://andreikara@bitbucket.org/andreikara/react_huntrss.git)*

**Реализовано** простое SPA, который позволяет просматривать фриланс проекты из RSS-ленты Freelancehunt. Главный экран приложения должен содержать следующие элементы:

**Таблица со списком проектов:**

1. Название проекта
2. Категории проекта
3. Бюджет
4. Фильтр
5. Поиск фразы в названии или описании проекта
6. Фильтр по бюджету проекта

**При нажатии на проект в таблице необходимо показать экран с деталями проекта:**

1. Название проекта
2. Описание проекта
3. Ссылка на ставки на Freelancehunt

**Дополнительно:**

1. Сборка Creat React App
2. React + Redux + devexpress react + jest test + Material UI
3. Наложены тысты на Бизнес логику. Путь с айлу тестирования ```src/store/reducers/rssProjects.test.js```
4. style components (новая технология которая вместо CSS/SCSS) ее активно внидряет Google в Material UI 

---

## Как Запустить

1. *[Скачать](https://andreikara@bitbucket.org/andreikara/react_huntrss.git)* проект или Клонировать.
2. В папку с проектом установить ```npm``` или ```yarn``` (Командой из Консоли ``` npm install / yarn install ```).
3. Запустить сборку **npm run start** или **yarn run start**.

---